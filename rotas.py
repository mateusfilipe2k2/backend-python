import random
import string

from flask import request, jsonify
from database import database
from app import appClass

client = database.client
db = database.db
users = database.users
locations = database.locations

app = appClass.create_app();

@app.route('/teste', methods=['GET'])
def teste():    
    return "teste"
    
@app.route('/AdicionarUsuario/', methods=['GET'])
def userAdd():
    args = request.args
    argUser = {
        "id": ''.join(random.sample(string.ascii_lowercase, 8)),
        "email": args["email"],
        "nome": args["nome"],
    }

    if(users.find_one( {"email": argUser["email"], "nome": argUser["nome"]} )):
        resp = jsonify(estado="jaExiste")
    else:
        users.insert_one(argUser)
        resp = jsonify(estado="criado")
    return resp

@app.route('/RemoverUsuario/', methods=['GET'])
def userRemove():
    args = request.args
    argUser = {
        "email": args["email"],
        "nome": args["nome"],
    }

    if(users.find_one_and_delete( {"email": argUser["email"]} )):
        resp = jsonify(estado="Deletado")
    else:
        resp = jsonify(estado="naoExiste")
    
    return resp

@app.route('/AdicionarPonto/', methods=['GET'])
def pontoAdd():
    args = request.args
    argLocation = {
        "id": ''.join(random.sample(string.ascii_lowercase, 8)),
        "latitude": args["latitude"],
        "longitude": args["longitude"],
        "email": args["email"],
    }
    if(users.find_one( {"email": argLocation["email"]} )):
        locations.insert_one(argLocation)
        resp = jsonify(estado="Adicionado")
    else:
        resp = jsonify(estado="emailInvalido")
        
    return resp
    
@app.route('/RemoverPonto/', methods=['GET'])
def pontoRemove():
    args = request.args
    argLocation = {
        "id": args["id"],
        "nome": args["user"],
    }
    if(users.find_one_and_delete( {"id": argLocation["id"], "nome": argLocation["nome"]} )):
        resp = jsonify(estado="deletado")
    else:
        resp = jsonify(estado="pontoInvalido")
        
    return resp
