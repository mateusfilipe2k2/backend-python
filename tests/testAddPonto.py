import requests

# pytest tests/testAddUser.py -v

def testAddUser():
    latitude = "latitude"
    longitude = "longitude"
    email = "email"
    response = requests.get("http://127.0.0.1:5000/AdicionarPonto/", params={"latitude": latitude, "longitude": longitude, "email": email})
    assert response.status_code == 200
