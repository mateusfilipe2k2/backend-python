#pytest /tests -v

# from flask import Flask
import pytest
from dadosGeograficos.src.app import appClass   


@pytest.fixture(scope = "session")
def app():
    app = appClass.create_app();
    return app
    # app = Flask(__name__)
    # app.config['SECRET_KEY'] = 'your secret key'
    # return app

