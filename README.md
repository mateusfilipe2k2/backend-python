# Projeto Backend Python

Projeto desenvolvido para o sprint4 do processo seletivo do terraLab. Desenvolvido utilizando flask, mongoDB e pytest como ferramentas.

    pip install
    pytest tests/testAddUser.py -v
    flask  run

<!-- export FLASK_APP=rotas
export FLASK_ENV=development -->